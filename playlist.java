import java.util.ArrayList;

class playlist {
    public static void main(String[] args) {
        new Player();
    }
}

class Artist {
    public String firstName;
    public String lastName;

    Artist(String ln, String fn) {
        this.firstName = fn;
        this.lastName = ln;
    }

    public String getFullName() {
        return this.lastName + ' ' + this.firstName;
    }
}

class Music {
    public String title;
    public int duration; // durée en secondes
    public Artist[] artistSet; // ensemble d'artistes

    Music(String titleI, int durationI, Artist[] artistSetI) {
        this.title = titleI;
        this.duration = durationI;
        this.artistSet = artistSetI;
    }

    public String getInfos() {
        String minutes = this.duration / 60 + "min " + this.duration % 60 + "sec";
        String Artistes = "";
        for (Artist artist : this.artistSet) {
            Artistes = Artistes + " " + artist.getFullName();
        }
        return this.title + " " + minutes + " par : " + Artistes;
    }

    public int getDuration() {
        return this.duration;
    }
}

class PlaylistList {
    public ArrayList<Music> musicList = new ArrayList<>();
    public Music currentMusic;

    public void addMusic(Music music) {
        musicList.add(music);
        if(currentMusic==null){
            this.currentMusic=this.musicList.get(0);
        }
    }

    public void remove(int position) {
      musicList.remove(position);
    }

    public void next() {
        if (this.musicList.size() > this.musicList.indexOf(currentMusic)+1) {
            this.currentMusic = this.musicList.get(this.musicList.indexOf(currentMusic)+1);
        } else {
            this.currentMusic = this.musicList.get(0);
        }
    }

    public String getTotalDuration() {
        int totalDuration = 0;
        for (Music music : musicList) {
            totalDuration = totalDuration + music.getDuration();
        }
        String minutes = totalDuration / 60 + "min " + totalDuration % 60 + "sec";
        return minutes;
    }

    public String getCurrentMusic() {
        return this.currentMusic.getInfos();
    }

}

class Player {
    Player() {
        // on definit quelques artistes
        Artist Goldman = new Artist("Jean-Jacques", "Goldman");
        Artist Angele = new Artist("Van Laeken", "Angèle");
        Artist Orelsan = new Artist("Aurélien", "Cotentin");
        Artist Stromae = new Artist("Paul", "Van Haverl");

        // on affiche deux artiste pour tester le get full name
        System.out.println(Goldman.getFullName());
        System.out.println(Stromae.getFullName());

        // on va creer quelque morceau a présent
        Music laPluie = new Music("La pluie", 177, new Artist[] { Orelsan, Stromae });
        Music ouiOuNon = new Music("Oui ou non", 216, new Artist[] { Angele });
        Music basic = new Music("Basic", 163, new Artist[] { Orelsan });
        Music pasToi = new Music("Pas toi", 244, new Artist[] { Goldman });
        Music evidemment = new Music("Évidemment", 206, new Artist[] { Orelsan, Angele });

        // on test le get info sur 2 musics
        System.out.println(ouiOuNon.getInfos());
        System.out.println(pasToi.getInfos());

        // On creer une playlist avec toutes les musiques
        PlaylistList MixedMusic = new PlaylistList();
        MixedMusic.addMusic(laPluie);
        MixedMusic.addMusic(ouiOuNon);
        MixedMusic.addMusic(basic);
        MixedMusic.addMusic(pasToi);
        MixedMusic.addMusic(evidemment);

        // on fait un get duration pour verifier qu'on a toute les musiques
        // a vu de nez on devrais avoir 15-18min min environs
        System.out.println(MixedMusic.getTotalDuration());
        // return 16min 46sec, donc add et getTotalduration ok !

        // on va supprimer Oui ou non, on devrais passer a 13 min 10sec
        MixedMusic.remove(1);
        System.out.println(MixedMusic.getTotalDuration());
        // remove ok !

        // on verifie que le current music est bien initialisé
        System.out.println(MixedMusic.getCurrentMusic());

        // Normalement on a 4 musique (donc de 0-3) on va next 2 musiques on devrais
        // donc tomber a 2
        MixedMusic.next();
        MixedMusic.next();
        System.out.println(MixedMusic.getCurrentMusic());

        // si on continue 2 autres fois on est censé retourné a 0
        MixedMusic.next();
        MixedMusic.next();
        System.out.println(MixedMusic.getCurrentMusic());
        // Next ok !

    }
}
